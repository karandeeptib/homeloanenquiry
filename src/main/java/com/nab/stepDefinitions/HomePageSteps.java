package com.nab.stepDefinitions;

import com.nab.enduservalidationsteps.EndUserValidationSteps;
import com.nab.util.Utility;

import cucumber.api.java.en.Given;

/**
 * Step definition class defining actions based on the scenario written in
 * Feature files
 * 
 * @author karan
 *
 */
public class HomePageSteps {
	EndUserValidationSteps endUserValidations = new EndUserValidationSteps();

	@Given("I am on NAB homepage to run my test (.*) with my data in (.*) sheet")
	public void i_am_on_NAB_homepage_to_run_my_test(String testName, String sheetName) throws Exception {
		Utility.getRowData(testName, sheetName);
		endUserValidations.nabHomePageLandingValidation();
	}

}
