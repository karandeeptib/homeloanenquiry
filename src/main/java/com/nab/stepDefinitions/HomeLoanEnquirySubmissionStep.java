package com.nab.stepDefinitions;

import com.nab.enduservalidationsteps.EndUserValidationSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Step definition class defining actions based on the scenario written in
 * Feature files
 * 
 * @author karan
 *
 */
public class HomeLoanEnquirySubmissionStep {
	EndUserValidationSteps endUserValidations = new EndUserValidationSteps();

	@When("I navigate to Home loan enquiry page And submit the home loan call back request form")
	public void i_navigate_to_Home_loan_enquiry_page() {
		endUserValidations.homeLoanPageLandingValidation();
		endUserValidations.homeLoanEnquiryPageNavigationValidation();
		endUserValidations.enquiryFormLandingPageValidation();
	}

	@Then("my enquiry is submitted successfully")
	public void my_enquiry_is_submitted_successfully() {
		endUserValidations.fillAndSubmitEnquiryCallBackFormValidation();
	}
}
