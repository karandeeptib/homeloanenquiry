package com.nab.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.nab.util.Constants;
import com.nab.util.WebEventListener;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * This base class is used to initialize the driver and config files.
 * @author karan
 *
 */
public class TestBase {
	
	private final static Logger log=Logger.getLogger(TestBase.class);

	public static WebDriver driver;
	public static Properties prop;
	public static WebDriverWait wait;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	
	public TestBase() {
		File f=new File("src/main/java/com/nab/config");
		File configFilePath=new File(f,"config.properties");
		prop=new Properties();
		try {
			FileInputStream file=new FileInputStream(configFilePath.getAbsolutePath());
			prop.load(file);
		} catch (FileNotFoundException e) {
			log.error("File Not found at path : " +configFilePath.getAbsolutePath());
			e.getMessage();
		} catch (IOException e) {
			log.error("IO Exception for file : " +configFilePath.getAbsolutePath());
			e.getMessage();
		}
	}
	
	public static void initialization() {
		String browserName=prop.getProperty("browser");
		File f=new File("driver");
		File chromeDriverPath=new File(f,"chromedriver.exe");
		File ieDriverPath=new File(f,"IEDriverServer.exe");
		
		if(browserName.equalsIgnoreCase("Chrome")) {
			//System.setProperty("webdriver.chrome.driver", chromeDriverPath.getAbsolutePath());
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
		} else if(browserName.equalsIgnoreCase("InternetExplorer")) {
			System.setProperty("webdriver.ie.driver", ieDriverPath.getAbsolutePath());
			driver=new InternetExplorerDriver();
		} else {
			log.error("Incorrect Browser selected, please configure browser in configuration file");
		}
		
		e_driver=new EventFiringWebDriver(driver);
		eventListener=new WebEventListener();
		//register the eventListener with the e_driver of EventFiringWebDriver
		e_driver.register(eventListener);
		driver=e_driver;
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(Constants.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(Constants.IMPLICIT_WAIT, TimeUnit.SECONDS);
		wait=new WebDriverWait(driver, Constants.EXPLICIT_WAIT);
		driver.get(prop.getProperty("url"));
	}
	
	
}
