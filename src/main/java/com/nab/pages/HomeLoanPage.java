package com.nab.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.nab.base.TestBase;
import com.nab.util.Utility;

/**
 * Page library for Home Loan Page. This class contains the page objects and
 * action classes relevant to the page
 * 
 * @author karan
 *
 */
public class HomeLoanPage extends TestBase {
	private final static Logger log = Logger.getLogger(HomeLoanPage.class);

	@FindBy(xpath = "//div[@id='experience']/div[2]/descendant::p[contains(text(),'Enquire about a new loan')]")
	WebElement enquireNewLoans;

	@FindBy(xpath = "//head[@class='at-element-marker']/title")
	WebElement HomeLoanPageTitle;

	public HomeLoanPage() {
		PageFactory.initElements(driver, this);
	}

	public String validateHomeLoanPageLoadSuccess() {
		wait.until(ExpectedConditions.visibilityOf(enquireNewLoans));
		return enquireNewLoans.getText();
	}

	public HomeLoanAssistantPage navigateToHomeLoanEnquiryPage() {
		log.info("Navigating to the home loan enquiry form");
		wait.until(ExpectedConditions.visibilityOf(enquireNewLoans));
		enquireNewLoans.click();
		return new HomeLoanAssistantPage();
	}
}
