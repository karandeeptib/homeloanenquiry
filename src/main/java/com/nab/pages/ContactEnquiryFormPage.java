package com.nab.pages;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.nab.base.TestBase;
import com.nab.util.Utility;

import junit.framework.Assert;

/**
 * Page library for Contact Enquiry Form Page. This class contains the page
 * objects and action classes relevant to the page
 * 
 * @author karan
 *
 */
public class ContactEnquiryFormPage extends TestBase {
	private final static Logger log = Logger.getLogger(ContactEnquiryFormPage.class);

	@FindBy(xpath = "//*[contains(text(),'NAB Contact Centre Call Back Form')]")
	WebElement label_FormHeading;

	@FindBy(xpath = "//*[@id='field-page-Page1-isExisting']/label[1]")
	WebElement btn_ExistingCustomerYes;

	@FindBy(xpath = "//*[@id='field-page-Page1-isExisting']/label[2]")
	WebElement btn_ExistingCustomerNo;

	@FindBy(id = "field-page-Page1-aboutYou-firstName")
	WebElement input_FirstName;

	@FindBy(id = "field-page-Page1-aboutYou-lastName")
	WebElement input_LastName;

	@FindBy(id = "field-page-Page1-aboutYou-phoneNumber")
	WebElement input_PhoneNumber;

	@FindBy(id = "field-page-Page1-aboutYou-email")
	WebElement input_Email;

	@FindBy(xpath = "//*[@id='label-page-Page1-aboutYou-state']/following-sibling::div")
	WebElement dropdown_SelectState;

	@FindBy(id = "page-Page1-btnGroup-submitBtn")
	WebElement btn_SubmitEnquiryForm;

	@FindBy(css = "div#react-select-3-option-1.css-1sjvff7-option.react-select__option")
	WebElement dropdown_SelectStateFromList;

	@FindBy(xpath = "//*[contains(text(),'There are errors on the page:')]")
	WebElement errorValidationmessage;

	@FindBy(xpath = "//*[contains(text(),'There are errors on the page:')]/../ul/li/a")
	List<WebElement> listOfErrors;

	public ContactEnquiryFormPage() {
		PageFactory.initElements(driver, this);
	}

	public String validateContactEnquiryFormPageTitle() {
		Utility.switchToNewWindow();
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("return document.readyState").equals("complete");
		wait.until(ExpectedConditions.visibilityOf(label_FormHeading));
		return label_FormHeading.getText();
	}

	public EnquiryFormSubmitPage FillFormDetailsValidation() {
		if(Utility.testData.get("IsExistingCustomer").equalsIgnoreCase("No")) {
		btn_ExistingCustomerNo.click();
		} else if(Utility.testData.get("IsExistingCustomer").equalsIgnoreCase("Yes")) {
			btn_ExistingCustomerYes.click();
		}
		input_FirstName.sendKeys(Utility.testData.get("FirstName"));
		input_LastName.sendKeys(Utility.testData.get("LastName"));
		input_PhoneNumber.sendKeys("0"+Utility.testData.get("PhoneNumber"));
		input_Email.sendKeys(Utility.testData.get("email"));
		dropdown_SelectState.click();
		WebElement selectStateFromList = driver.findElement(By.cssSelector("div#react-select-3-option-"
				+ Utility.stateToIdMapping().get("NSW") + ".css-1sjvff7-option.react-select__option"));
		wait.until(ExpectedConditions.visibilityOf(btn_ExistingCustomerNo));
		performClickOnElement(Utility.testData.get("State"), selectStateFromList);
		Utility.scrollIntoView(btn_SubmitEnquiryForm);
		btn_SubmitEnquiryForm.click();
		inputFieldValidationCheck();
		return new EnquiryFormSubmitPage();
	}

	public void performClickOnElement(String state, WebElement element) {
		WebElement dropdown_selectStateFromList = element;
		dropdown_selectStateFromList.click();
	}

	public void inputFieldValidationCheck() {
		log.info("Checking if there are any validation errors on page");
		if (isPresent(errorValidationmessage)) {
			log.info("There are errors on page");
			for (int i = 0; i < listOfErrors.size(); i++) {
				String errorOnField = listOfErrors.get(i).getText();
				log.info("Form Field = \"" + errorOnField
						+ "\" has invalid or missing data, please enter the correct data  ");
				System.out.println("Form Field = \"" + errorOnField
						+ "\" has invalid or missing data, please enter the correct data  ");
			}
			Assert.assertTrue("Form Field have invalid or missing data, please enter the correct data  ", false);
			driver.quit();
		} else {
			log.info("There are no field level validation errors on page");
			Assert.assertTrue("There are no field level validation errors on page", true);
		}
	}

	public Boolean isPresent(WebElement element) {
		try {
			element.isEnabled();
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
}
