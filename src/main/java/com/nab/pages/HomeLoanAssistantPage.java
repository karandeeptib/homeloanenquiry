package com.nab.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.nab.base.TestBase;
import com.nab.util.Utility;

/**
 * Page library for Home Loan Assistant Page. 
 * This class contains the page objects and action classes relevant to the page
 * @author karan
 *
 */
public class HomeLoanAssistantPage extends TestBase{
	private final static Logger log=Logger.getLogger(HomeLoanAssistantPage.class);
	
	 By rootElement=By.xpath("//div[@id='contact-form-shadow-root']");
	 By NewHomeLoanRadioOption=By.id("myRadioButton-0");
	 By NextButton=By.cssSelector("button.Buttonstyle__StyledButton-sc-1vu4swu-3.cchfek");
		
	 
	public String validateHomeAssistantPageTitle() {
		JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
		jsExecutor.executeScript("return document.readyState").equals("complete");
		try {
			System.out.println("Implmeneting wait 3 sec");
			Utility.implicitUserWait(3);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return driver.getTitle();
	}
	
	
	public ContactEnquiryFormPage selectNewHomeLoansAndContinue(){
		try {
			System.out.println("Implmeneting wait 1 sec");
			Utility.implicitUserWait(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		log.info("Selecting home loan enquiry option and continue");
		System.out.println("Selecting home loan enquiry option and continue");
		WebElement shadowRootElement= Utility.accessRootElement(driver.findElement(rootElement));
		WebElement optionNewHomeLoan=shadowRootElement.findElement(NewHomeLoanRadioOption);
		optionNewHomeLoan.click();
		WebElement buttonNext=shadowRootElement.findElement(NextButton);
		Utility.scrollIntoView(buttonNext);
		buttonNext.click();
		return new ContactEnquiryFormPage();
		}

}
