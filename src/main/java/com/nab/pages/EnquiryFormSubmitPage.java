package com.nab.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.nab.base.TestBase;

/**
 * Page library for Enquiry form submit Page. This class contains the page
 * objects and action classes relevant to the page
 * 
 * @author karan
 *
 */
public class EnquiryFormSubmitPage extends TestBase {
	private final static Logger log = Logger.getLogger(EnquiryFormSubmitPage.class);
	public String applicantName = null;

	@FindBy(xpath = "//*[@id='page-outcome-rowthankyoupageheader-columns3-thankyoupageheadercontent1']/h3")
	WebElement label_ThankYouMessage;

	@FindBy(xpath = "//*[contains(text(),' RECEIVED YOUR REQUEST')]")
	WebElement label_RequestReceivedMessage;

	public EnquiryFormSubmitPage() {
		PageFactory.initElements(driver, this);
	}

	public String validateSuccessMessage() {
		wait.until(ExpectedConditions.visibilityOf(label_RequestReceivedMessage));
		return label_RequestReceivedMessage.getText();
	}

	public void validateApplicantName() {
		String thankYouText[] = label_ThankYouMessage.getText().split(",");
		System.out.println(thankYouText[1]);
		applicantName = thankYouText[1];
		driver.quit();
	}
}
