package com.nab.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.nab.base.TestBase;

/**
 * Page library for NAB Home Page. This class contains the page objects and
 * action classes relevant to the page
 * 
 * @author karan
 *
 */
public class HomePage extends TestBase {
	private final static Logger log = Logger.getLogger(TestBase.class);

	@FindBy(xpath = "//*[contains(text(),'Our range of personal products')]/../following::li[6]/a")
	WebElement Lnk_HomeLoan;

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public String validateHomePageTitle() {
		log.info("Fetching Home Loan Page title : " + driver.getTitle());
		return driver.getTitle();
	}

	public HomeLoanPage accessHomeLoan() {
		log.info("Clicking on Home Loan link");
		Lnk_HomeLoan.click();
		return new HomeLoanPage();
	}
}
