package com.nab.enduservalidationsteps;

import com.nab.base.TestBase;
import com.nab.pages.ContactEnquiryFormPage;
import com.nab.pages.EnquiryFormSubmitPage;
import com.nab.pages.HomeLoanAssistantPage;
import com.nab.pages.HomeLoanPage;
import com.nab.pages.HomePage;
import com.nab.util.Constants;
import com.nab.util.Utility;

import junit.framework.Assert;

/**
 * This Class is used to define the test execution flow and assert the test conditions.
 * @author karan
 *
 */
@SuppressWarnings("deprecation")
public class EndUserValidationSteps extends TestBase {
	HomePage homePage = new HomePage();
	HomeLoanPage homeLoanPage;
	HomeLoanAssistantPage homeLoanAssistantPage;
	ContactEnquiryFormPage contactEnquiryFormPage;
	EnquiryFormSubmitPage enquiryFormSubmitPage;

	public void nabHomePageLandingValidation() {
		TestBase.initialization();
		String homePageTitle = homePage.validateHomePageTitle();
		System.out.println(homePageTitle);
		Assert.assertEquals(Constants.HOMEPAGE_TITLE, homePageTitle);
	}

	public void homeLoanPageLandingValidation() {
		homeLoanPage = homePage.accessHomeLoan();
		String homeLoanPageLoadSuccess = homeLoanPage.validateHomeLoanPageLoadSuccess();
		System.out.println(homeLoanPageLoadSuccess);
		Assert.assertEquals(Constants.HOMELOAN_LOAD_SUCCESS_CHECK_MESSAGE, homeLoanPageLoadSuccess);
	}

	public void homeLoanEnquiryPageNavigationValidation() {
		homeLoanAssistantPage = homeLoanPage.navigateToHomeLoanEnquiryPage();
		String homeLoanAssistantPageTitle = homeLoanAssistantPage.validateHomeAssistantPageTitle();
		System.out.println(homeLoanAssistantPageTitle);
		Assert.assertEquals(Constants.HOMELOAN_ASSISTANCE_DIRECTORY_TITLE, homeLoanAssistantPageTitle);
	}

	public void enquiryFormLandingPageValidation() {
		contactEnquiryFormPage = homeLoanAssistantPage.selectNewHomeLoansAndContinue();
		String enquiryFormTitle = contactEnquiryFormPage.validateContactEnquiryFormPageTitle();
		System.out.println(enquiryFormTitle);
		Assert.assertEquals(Constants.ENQUIRY_FORM_HEADING, enquiryFormTitle);
	}

	public void fillAndSubmitEnquiryCallBackFormValidation() {
		enquiryFormSubmitPage = contactEnquiryFormPage.FillFormDetailsValidation();
		String enquiryFormSubmissionSuccessMessage = enquiryFormSubmitPage.validateSuccessMessage();
		Assert.assertEquals(Constants.ENQUIRY_FORM_SUBMISSION_SUCCESS_MESSAGE, enquiryFormSubmissionSuccessMessage);
		enquiryFormSubmitPage.validateApplicantName();
		String equiryFormSubmissionPageApplicationNameDisplayed = enquiryFormSubmitPage.applicantName.trim();
		Assert.assertEquals(Utility.testData.get("FirstName"), equiryFormSubmissionPageApplicationNameDisplayed);

	}
}
