package com.nab.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/main/resources/HomeLoanEnquiry.feature",
		glue = "com.nab.stepDefinitions",
		monochrome = true,
		plugin= {"pretty","html:test-outout", "json:json_output/cucumber.json", "junit:junit_xml/cucumber.xml"})

/**
 * TestRunner class used to execute the Cucumber test
 * @author karan
 *
 */
public class TestRunner {
}
