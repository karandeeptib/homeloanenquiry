package com.nab.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nab.base.TestBase;

/**
 * This class contains the utility methods that are commonly used during the
 * execution of the project
 * 
 * @author Karandeep
 *
 */
public class Utility extends TestBase {
	public final static Logger log = LoggerFactory.getLogger(Utility.class);
	public static HashMap<String, String> testData = new HashMap<String, String>();

	public static WebElement accessRootElement(WebElement element) {
		WebElement shadowElement = (WebElement) ((JavascriptExecutor) driver)
				.executeScript("return arguments[0].shadowRoot", element);
		return shadowElement;
	}

	public static void switchToNewWindow() {
		Set<String> handles = driver.getWindowHandles();
		if (handles.size() > 1) {
			for (String newWindow : handles) {
				log.info("New Window is present : " + newWindow);
				driver.switchTo().window(newWindow);
			}
		}
	}

	public static void scrollIntoView(WebElement element) {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].scrollIntoView()", element);
	}

	public static void implicitUserWait(int seconds) throws InterruptedException {
		Thread.sleep(seconds * 1000);
	}

	public static HashMap<String, Integer> stateToIdMapping() {
		HashMap<String, Integer> statetoIdMap = new HashMap<String, Integer>();
		statetoIdMap.put("ACT", 0);
		statetoIdMap.put("NSW", 1);
		statetoIdMap.put("NT", 2);
		statetoIdMap.put("QLD", 3);
		statetoIdMap.put("SA", 4);
		statetoIdMap.put("TAS", 5);
		statetoIdMap.put("VIC", 6);
		statetoIdMap.put("WA", 7);
		return statetoIdMap;
	}

	public static HashMap<String, String> getRowData(String rowKeyword, String SheetName) throws Exception {
		File f = new File("src/main/java/com/nab/testdata");
		File testDataFilePath = new File(f, "HomeLoanEnquiryFormData.xlsx");
		FileInputStream file = new FileInputStream(testDataFilePath.getAbsolutePath());
		System.out.println("Keyword is : " + rowKeyword);
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		System.out.println("SheetName is : " + SheetName);
		XSSFSheet sheet = workbook.getSheet(SheetName);
		DataFormatter objDefaultFormat1 = new DataFormatter();
		FormulaEvaluator objFormulaEvaluator1 = new XSSFFormulaEvaluator(workbook);
		int rowCount = sheet.getPhysicalNumberOfRows();
		System.out.println("Total number of rows are : " + rowCount);
		int columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Total number of columns are : " + columnCount);
		for (int i = 0; i < rowCount; i++) {
			System.out.println("Test Case Name is : " + sheet.getRow(i).getCell(0).toString());
			if (sheet.getRow(i).getCell(0).toString().equals(rowKeyword)) {
				for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {

					Cell cellValue = sheet.getRow(i).getCell(k);
					objFormulaEvaluator1.evaluate(cellValue);
					String cellValueStr = objDefaultFormat1.formatCellValue(cellValue, objFormulaEvaluator1);
					System.out.println("Data Key is : " + sheet.getRow(0).getCell(k).toString() + "Data Value is : "
							+ cellValueStr);

					testData.put(sheet.getRow(0).getCell(k).toString(), cellValueStr);
				}
				break;
			}
		}
		file.close();
		System.out.println("Test Data is : " + testData);
		return testData;
	}

	public static void takeScreenshotAtEndOfTest() throws IOException {
		File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String currentDir = System.getProperty("user.dir");

		FileUtils.copyFile(screenshotFile, new File(currentDir + "/screenshot/" + System.currentTimeMillis() + ".png"));
	}

}
