package com.nab.util;

/**
 * This class defines all teh constant values used through the project
 * 
 * @author karan
 *
 */
public class Constants {

	public static final int PAGE_LOAD_TIMEOUT = 20;
	public static final int IMPLICIT_WAIT = 3;
	public static final int EXPLICIT_WAIT = 20;
	public static final String HOMEPAGE_TITLE = "NAB Personal Banking - insurance, loans, accounts, credit cards - NAB";
	public static final String HOMELOAN_LOAD_SUCCESS_CHECK_MESSAGE = "Enquire about a new loan";
	public static final String HOMELOAN_ASSISTANCE_DIRECTORY_TITLE = "Customer assistance directory | We’re here to help - NAB";
	public static final String ENQUIRY_FORM_HEADING = "NAB CONTACT CENTRE CALL BACK FORM";
	public static final String ENQUIRY_FORM_SUBMISSION_SUCCESS_MESSAGE = "WE'VE RECEIVED YOUR REQUEST";

}
