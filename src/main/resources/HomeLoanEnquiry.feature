Feature: HomeLoan Enquiry

Scenario Outline: Validate and submit Homeloan enquiry call back form

Given I am on NAB homepage to run my test <TestName> with my data in <TestSheetName> sheet 
When I navigate to Home loan enquiry page And submit the home loan call back request form
Then my enquiry is submitted successfully

Examples:
|TestName|TestSheetName|
|Test1|UserData|
|Test2|UserData|
