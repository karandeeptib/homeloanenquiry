# README #

This is the project for Web UI Automation using POM design pattern with Data driven approach

### Tools/Framework/Libraries ###

- Maven - build tool
- Junit - test runner
- Cucumber - BDD/Gherkin style feature files
- Design pattern - Page Object Model
- Design Approach - Data driven using external data source
- Evidences - Screenshot on Failure
- Logging - Log4j loggers
- Listeners - WebDriverEvent Listener

### How to run the test ###

- Clone the project into local directory
- Import project into your workspace
- Run the Junit test from the TestRunner file of the cucumber test

### How the project works ###

- All the test cases are defined in the feature file in the form of Given, When, Then scenarios in src/main/resources
- The test input data is managed in the external data source excell file which is picked during test based on the 
	test case name specified in Sceanrio examples in the feature files.
- A step definition file provides the implementation of test steps written in the feture file in src/main/java
- Step definition file interacts with the middle layer enduserstep which defines the test execution steps and assert test conditions
- All the Web page related data such as object repositiroy and action methods are stored in page library file
- Base class in the project initialized the web driver and properties before test execution starts.
- Utility file contains all the necessary utilites required for test executions.
- Execution logs are generated using log4j and are stored in application.log file
- For failed test cases/ exceptions, screenshots are generated and stored in the screenshot folder
