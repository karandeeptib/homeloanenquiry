$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/resources/HomeLoanEnquiry.feature");
formatter.feature({
  "line": 1,
  "name": "HomeLoan Enquiry",
  "description": "",
  "id": "homeloan-enquiry",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Validate and submit Homeloan enquiry call back form",
  "description": "",
  "id": "homeloan-enquiry;validate-and-submit-homeloan-enquiry-call-back-form",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I am on NAB homepage to run my test \u003cTestName\u003e with my data in \u003cTestSheetName\u003e sheet",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to Home loan enquiry page And submit the home loan call back request form",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "my enquiry is submitted successfully",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "homeloan-enquiry;validate-and-submit-homeloan-enquiry-call-back-form;",
  "rows": [
    {
      "cells": [
        "TestName",
        "TestSheetName"
      ],
      "line": 10,
      "id": "homeloan-enquiry;validate-and-submit-homeloan-enquiry-call-back-form;;1"
    },
    {
      "cells": [
        "Test1",
        "UserData"
      ],
      "line": 11,
      "id": "homeloan-enquiry;validate-and-submit-homeloan-enquiry-call-back-form;;2"
    },
    {
      "cells": [
        "Test2",
        "UserData"
      ],
      "line": 12,
      "id": "homeloan-enquiry;validate-and-submit-homeloan-enquiry-call-back-form;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "Validate and submit Homeloan enquiry call back form",
  "description": "",
  "id": "homeloan-enquiry;validate-and-submit-homeloan-enquiry-call-back-form;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I am on NAB homepage to run my test Test1 with my data in UserData sheet",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to Home loan enquiry page And submit the home loan call back request form",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "my enquiry is submitted successfully",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Test1",
      "offset": 36
    },
    {
      "val": "UserData",
      "offset": 58
    }
  ],
  "location": "HomePageSteps.i_am_on_NAB_homepage_to_run_my_test(String,String)"
});
formatter.result({
  "duration": 23173809200,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySubmissionStep.i_navigate_to_Home_loan_enquiry_page()"
});
formatter.result({
  "duration": 27789650900,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySubmissionStep.my_enquiry_is_submitted_successfully()"
});
formatter.result({
  "duration": 11537965900,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Validate and submit Homeloan enquiry call back form",
  "description": "",
  "id": "homeloan-enquiry;validate-and-submit-homeloan-enquiry-call-back-form;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I am on NAB homepage to run my test Test2 with my data in UserData sheet",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to Home loan enquiry page And submit the home loan call back request form",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "my enquiry is submitted successfully",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Test2",
      "offset": 36
    },
    {
      "val": "UserData",
      "offset": 58
    }
  ],
  "location": "HomePageSteps.i_am_on_NAB_homepage_to_run_my_test(String,String)"
});
